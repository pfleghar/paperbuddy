//
//  PBPDFDocument.m
//  PaperBuddy
//
//  Created by Ralf Pfleghar on 27.02.13.
//  Copyright (c) 2013 Ralf Pfleghar. All rights reserved.
//

#import "PBPDFDocument.h"
#import "PBDocumentTypeController.h"

@implementation PBPDFDocument

@synthesize documentDate;
@synthesize documentTitle;
@synthesize documentNotebook;
@synthesize documentTags;
@synthesize documentType;
@synthesize documentUrl;

-(id) initWithURL:(NSURL *)url {
    
    if (self = [super initWithURL:url]) {
        self.documentUrl = url;
        
        // lets find out some things about document
        [self determineDocumentTitle];
        [self determineDocumentDate];
        [self determineGeneralTags];
        
        // Let's see if we can determine a type
        [self determineDocumentType];
        
        // let's apply the type and add generals tags again
        [self applyDocumentType: false];
        
    }
    return self;
    
}

- (NSMutableArray *) documentTagsAsArray {
    NSMutableArray *source = [[NSMutableArray alloc] initWithArray:[[self documentTags] componentsSeparatedByString:@","]];
    NSMutableArray *result = [[NSMutableArray alloc] init];
    for (int i=0; i < [source count]; i++) {
        if (![[source objectAtIndex:i] isEqualToString:@""]) {
            [result addObject:[source objectAtIndex:i]];
        }
    }
    return result;
}

- (void) determineDocumentType {
    PBDocumentTypeController *documentTypeController = [PBDocumentTypeController sharedInstance];
    
    
    // find matching Document Types
    int finalIndex = -1;
    int winnerResult = 0;
    
    for (int y=0; y<[[documentTypeController types] count]; y++) {
        NSDictionary *object = [[documentTypeController types] objectAtIndex:y];
        int howManyFound = 0;
        
        NSArray *words = [object objectForKey:@"Keywords"];
        BOOL allFound = true;
        
        for(int i=0;i<[words count];i++) {
            NSArray *results = [self findString:[words objectAtIndex:i] withOptions:0];
            if ([results count] == 0 ) {
                allFound = false;
            } else {
                howManyFound = howManyFound + (int) [results count];
            }
        }
        
        if (allFound && howManyFound> 0 && howManyFound>winnerResult) {
            winnerResult = howManyFound;
            finalIndex = (int) y;
        }
        
    }
    
    
    // Found DocumentType, so set it or not
    if (finalIndex != -1) {
        DebugLog(@"found document type: %@", [[[documentTypeController types] objectAtIndex:(int)finalIndex] objectForKey:@"Name"]);
        [self setDocumentType: [[documentTypeController types] objectAtIndex:(int)finalIndex]];
        
    } else {
        [self setDocumentType:nil];
        
    }
    
    
    
    
}


- (void) determineGeneralTags{
    
    
    NSString *generalTags = [[NSUserDefaults standardUserDefaults] stringForKey:@"GeneralTags"];
    NSArray *gTagsArray = [[NSArray alloc] initWithArray:[generalTags componentsSeparatedByString:@","]];
    
    if (![self documentTags]) {
        [self setDocumentTags:@""];
    }
    
    if (!gTagsArray) return;
    for (int i=0; i< [gTagsArray count]; i++) {
        NSArray *results = [self findString:[gTagsArray objectAtIndex:i] withOptions:0];
        if ([results count] > 0 ) {
            NSMutableString *tags = [[NSMutableString alloc] initWithString:[self documentTags]];
            [tags appendString:@","];
            [tags appendString:[gTagsArray objectAtIndex:i]];
            [self setDocumentTags:tags];
        }
    }
}


- (void) applyDocumentType: (BOOL) forceTitle {
    
    if ([self documentType]) {
        [self setDocumentTags: [[[self documentType] objectForKey:@"Tags"]  componentsJoinedByString:@","]];
        [self setDocumentNotebook:[[self documentType] objectForKey:@"Notebook"]];
        
        // check type for default title
        if ([[self documentTitle] isEqualToString:@"Untitled"] ||  [[self documentType] objectForKey:@"Title"]==nil || forceTitle) {
            self.documentTitle = [[self documentType] objectForKey:@"Title"];
        }

        [self determineGeneralTags];
        
    }
    
}




- (void) determineDocumentDate {

    NSDate *currentDate;
    unsigned long currentIndex;
    
    {
        // Check for long version
        NSRegularExpression *regexLong = [NSRegularExpression regularExpressionWithPattern:@"(\\d{2}\\. (Januar|Februar|März|April|Mai|Juni|Juli|August|September|Oktober|November|Dezember) \\d{4})" options:NSRegularExpressionCaseInsensitive error:NULL];
        NSTextCheckingResult *newSearchStringLong = [regexLong firstMatchInString:[self string] options:0 range:NSMakeRange(0, [[self string] length])];
        NSString *substrLong = [[self string] substringWithRange:newSearchStringLong.range];
        DebugLog(@"Date String Long: %@ at index %lu", substrLong, (unsigned long)[newSearchStringLong range].location);
        
        NSDateFormatter *dateFormatterLong = [[NSDateFormatter alloc] init];
        [dateFormatterLong setDateStyle:NSDateFormatterLongStyle]; // z.B. 08.10.2008
        [dateFormatterLong setTimeStyle:NSDateFormatterNoStyle]; // keine Zeitangabe
        
        NSDate *documentDateLong = [dateFormatterLong dateFromString:substrLong];
        
        if (documentDateLong !=nil) {
            currentDate = documentDateLong;
            currentIndex = (unsigned long)[newSearchStringLong range].location;
            DebugLog(@"Date Long: %@ added", [documentDateLong description]);
        }
        
    }

    {
        // Check for Medium Version
        NSRegularExpression *regexMedium = [NSRegularExpression regularExpressionWithPattern:@"(\\d{2}\\.\\d{2}\\.\\d{4})" options:NSRegularExpressionCaseInsensitive error:NULL];
        NSTextCheckingResult *newSearchStringMedium = [regexMedium firstMatchInString:[self string] options:0 range:NSMakeRange(0, [[self string] length])];
        NSString *substrMedium = [[self string] substringWithRange:newSearchStringMedium.range];
        DebugLog(@"Date String Medium: %@ at index %lu", substrMedium,  (unsigned long)[newSearchStringMedium range].location);
        
        NSDateFormatter *dateFormatterMedium = [[NSDateFormatter alloc] init];
        [dateFormatterMedium setDateStyle:NSDateFormatterMediumStyle]; // z.B. 08.10.2008
        [dateFormatterMedium setTimeStyle:NSDateFormatterNoStyle]; // keine Zeitangabe
        
        NSDate *documentDateMedium = [dateFormatterMedium dateFromString:substrMedium];
        
        if ((documentDateMedium !=nil) && ([newSearchStringMedium range].location < currentIndex)) {
            currentDate = documentDateMedium;
            currentIndex = (unsigned long)[newSearchStringMedium range].location;
            DebugLog(@"Date Medium: %@ added", [documentDateMedium description]);
        }
    }

    
    
    {
        // Check for Short Version
        NSRegularExpression *regexShort = [NSRegularExpression regularExpressionWithPattern:@"(\\d{2}\\.\\d{2}\\.\\d{2})" options:NSRegularExpressionCaseInsensitive error:NULL];
        NSTextCheckingResult *newSearchString = [regexShort firstMatchInString:[self string] options:0 range:NSMakeRange(0, [[self string] length])];
        NSString *substrShort = [[self string] substringWithRange:newSearchString.range];
        DebugLog(@"Date String Short: %@ at index %lu", substrShort,  (unsigned long)[newSearchString range].location);
        
        NSDateFormatter *dateFormatterShort = [[NSDateFormatter alloc] init];
        [dateFormatterShort setDateStyle:NSDateFormatterShortStyle]; // z.B. 08.10.08
        [dateFormatterShort setTimeStyle:NSDateFormatterNoStyle]; // keine Zeitangabe
        
        NSDate *documentDateShort = [dateFormatterShort dateFromString:substrShort];
        
        if ((documentDateShort !=nil) && ([newSearchString range].location < currentIndex)) {
            currentDate = documentDateShort;
            currentIndex = (unsigned long)[newSearchString range].location;
            DebugLog(@"Date Short: %@ added", [documentDateShort description]);
        }
    }
    
    // decide for newer date
    if (currentDate == nil) {
        [self setDocumentDate:[[NSDate alloc] init]];
    } else {
        [self setDocumentDate:currentDate];
    }
    
}



- (void) determineDocumentTitle {
    
    // get attributes
    NSDictionary *attr = [self documentAttributes];
    
    // define Title
    NSString *title = [attr objectForKey:@"Title"];
    NSArray *keywords = [attr objectForKey:@"Keywords"];
    
    if (title != nil && ![title isEqualToString:@""]) {
        self.documentTitle = title;

    } else if (keywords != nil && [keywords count] > 0) {
        self.documentTitle = [self readArray:keywords];
    } else {
        self.documentTitle = @"Untitled";
    }
    
    
    // trim title
    while ([self.documentTitle rangeOfString:@"  "].location != NSNotFound) {
        self.documentTitle = [self.documentTitle stringByReplacingOccurrencesOfString:@"  " withString:@" "];
    }

}

- (NSString *)readArray: (NSArray *) array
{
    NSMutableString * result = [[NSMutableString alloc] init];
    for (NSObject * obj in array)
    {
        [result appendString:[obj description]];
        [result appendString:@" "];
    }
    return result;
}


@end
