//
//  PBEvernoteController.h
//  PaperBuddy
//
//  Created by Ralf Pfleghar on 11.03.13.
//  Copyright (c) 2013 Ralf Pfleghar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PBEvernoteController : NSObject

@property NSMutableArray    *notebooks;
@property NSArray           *tags;


+ (PBEvernoteController*)sharedInstance;
- (void) synchronizeWithEvernote:(id)sender;
- (NSArray *) notebookNames;


- (BOOL) hasNotebookWithName: (NSString *) name;
- (NSString *) guidForName: (NSString *) name;
- (BOOL) hasTagWithName: (NSString *) tag;
- (NSString *) defaultNotebook;
- (NSArray *) tagsAsStrings;

    @end
