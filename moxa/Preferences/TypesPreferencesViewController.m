
#import "TypesPreferencesViewController.h"
#import "PBDocumentTypeController.h"
#import "PBEvernoteController.h"

@implementation TypesPreferencesViewController

@synthesize tableView;
PBDocumentTypeController *documentTypeController;

- (id)init
{
    return [super initWithNibName:@"TypesPreferencesView" bundle:nil];
    
}

- (void)awakeFromNib
{
    documentTypeController  = [PBDocumentTypeController sharedInstance];
    [tableView setDataSource:documentTypeController];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(evernoteSynchingFinished:)
                                                 name:@"EvernoteSynchingFinished" object:nil];
    [tableView registerForDraggedTypes: [NSArray arrayWithObject:moxaDocumentTypeTableData]];
    
    [self updatePanel];
}

#pragma mark -
#pragma mark MASPreferencesViewController


- (void)evernoteSynchingFinished:(NSNotification *)notification {
    DebugLog(@"hit me");
}

- (NSString *)identifier
{
    return @"TypesPreferences";
}

- (NSImage *)toolbarItemImage
{
    return [NSImage imageNamed:@"282-cards"];
}

- (NSString *)toolbarItemLabel
{
    return NSLocalizedString(@"Types", @"Toolbar item name for the People preference pane");
}

- (void) updatePanel {
    
    [[self notebookPopup] removeAllItems];
    [[self notebookPopup] addItemsWithTitles:[[PBEvernoteController sharedInstance] notebookNames]];
    
    if ([documentTypeController currentType]) {
        [[self notebookPopup] setEnabled:true];
        
        [[self tagsField] setEnabled:true];
        [[self keywordsField] setEnabled:true];
        [[self removeTypeButton] setEnabled:true];
        [[self titleField] setEnabled:true];
        
        [[self tagsField] setStringValue:[[[documentTypeController currentType] objectForKey:@"Tags"] componentsJoinedByString:@","]];
        [[self keywordsField] setStringValue:[[[documentTypeController currentType] objectForKey:@"Keywords"] componentsJoinedByString:@","]];
        [[self titleField] setStringValue:[[documentTypeController currentType] objectForKey:@"Title"]];

        
        if ([[PBEvernoteController sharedInstance] hasNotebookWithName:[[documentTypeController currentType] objectForKey:@"Notebook"]]) {
            [[self notebookPopup] selectItemWithTitle:[[documentTypeController currentType] objectForKey:@"Notebook"]];
        } else {
            [[self notebookPopup] selectItemWithTitle:[[PBEvernoteController sharedInstance] defaultNotebook]];
            
        }


    } else {
        [[self tagsField] setStringValue:@""];
        [[self keywordsField] setStringValue:@""];
        [[self titleField] setStringValue:@""];
        [[self notebookPopup] selectItemWithTitle:[[PBEvernoteController sharedInstance] defaultNotebook]];

        [[self notebookPopup] setEnabled:false];
        [[self titleField] setEnabled:false];
        [[self tagsField] setEnabled:false];
        [[self keywordsField] setEnabled:false];
        [[self removeTypeButton] setEnabled:false];
    }
}

- (IBAction)changeCurrentType:(id)sender {
    DebugLog(@"changing type");
    [[PBDocumentTypeController sharedInstance] setCurrentTypeByNumber:[tableView selectedRow]];
    [self updatePanel];
}

- (IBAction)addType:(id)sender {
   
    [documentTypeController addType];
    [tableView reloadData];
    [self updatePanel];
}

- (IBAction)removeType:(id)sender {
    
    [[documentTypeController types] removeObjectAtIndex:[tableView selectedRow]];
    [documentTypeController synchronizeTypes];
    [tableView reloadData];
    
    [self updatePanel];

    
}

- (IBAction)notebookChanged:(id)sender {
    DebugLog(@"changing notebook");
    NSMutableDictionary *new = [[NSMutableDictionary alloc] initWithDictionary:[documentTypeController currentType]];
    [new setObject:[[self notebookPopup] titleOfSelectedItem] forKey:@"Notebook"];
    
    [documentTypeController updateCurrentType: new];
    
    [documentTypeController synchronizeTypes];

}


- (void)controlTextDidChange:(NSNotification *)aNotification {
    NSMutableDictionary *new = [[NSMutableDictionary alloc] initWithDictionary:[documentTypeController currentType]];
    
    
    [new setObject:[[[self tagsField] stringValue] componentsSeparatedByString:@","] forKey:@"Tags"];
    [new setObject:[[[self keywordsField] stringValue] componentsSeparatedByString:@","] forKey:@"Keywords"];
    [new setObject:[[self notebookPopup] stringValue] forKey:@"Notebook"];
    [new setObject:[[self titleField] stringValue] forKey:@"Title"];
    
    [documentTypeController updateCurrentType: new];

    [documentTypeController synchronizeTypes];

}


@end
