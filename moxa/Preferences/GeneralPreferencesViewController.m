
#import "GeneralPreferencesViewController.h"
#import "EvernoteSDK.h"
#import "PBEvernoteController.h"

@implementation GeneralPreferencesViewController

@synthesize folderPathControl;
@synthesize folderCheckbox;

- (id)init
{
    return [super initWithNibName:@"GeneralPreferencesView" bundle:nil];
}

#pragma mark -
#pragma mark MASPreferencesViewController

- (NSString *)identifier
{
    return @"GeneralPreferences";
}

- (NSImage *)toolbarItemImage
{
    return [NSImage imageNamed:@"106-sliders"];
}

- (NSString *)toolbarItemLabel
{
    return NSLocalizedString(@"General", @"Toolbar item name for the General preference pane");
}


- (void)awakeFromNib
{
   
    NSError *error = nil;
    BOOL bookmarkDataIsStale;
    NSURL *bookmarkFileURL = nil;
    NSData *bookmarkData;
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"MoveFolderData"]) {
        bookmarkData = [[NSUserDefaults standardUserDefaults] objectForKey:@"MoveFolderData"];
    }
    
    bookmarkFileURL = [NSURL
                       URLByResolvingBookmarkData:bookmarkData
                       options:NSURLBookmarkResolutionWithSecurityScope
                       relativeToURL:nil
                       bookmarkDataIsStale:&bookmarkDataIsStale
                       error:&error];
    
    
    if (bookmarkFileURL) {
        [folderPathControl setURL:bookmarkFileURL];
    }
    
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"MoveFolderMode"]) {
        [folderCheckbox setState:[[NSUserDefaults standardUserDefaults] integerForKey:@"MoveFolderMode"]];
    }
    
}

- (IBAction)logoutFromEvernote:(id)sender {

    [[EvernoteSession sharedSession] logout];
        [[PBEvernoteController sharedInstance] synchronizeWithEvernote:self];
}



- (IBAction)chooseFolder:(id)sender {
    
    NSError *error = nil;
    NSData *bookmarkData = nil;

    
    NSOpenPanel* openDlg = [NSOpenPanel openPanel];
    [openDlg setCanChooseFiles:NO];
    [openDlg setCanChooseDirectories:YES];
    if ( [openDlg runModal] == NSOKButton )
    {
        NSURL* fileUrl = [openDlg URL];
        [[self folderPathControl] setURL:fileUrl];
        
        
        bookmarkData = [fileUrl
                        bookmarkDataWithOptions:NSURLBookmarkCreationWithSecurityScope
                        includingResourceValuesForKeys:nil
                        relativeToURL:nil
                        error:&error];
        
    }
    [[NSUserDefaults standardUserDefaults] setObject:bookmarkData  forKey:@"MoveFolderData"];
    [[NSUserDefaults standardUserDefaults] synchronize];

}


- (IBAction)moveChanged:(id)sender {
    DebugLog(@"changed");
    
    while ([folderCheckbox state] && ![[NSUserDefaults standardUserDefaults] objectForKey:@"MoveFolderData"]) {
        [self chooseFolder:nil];
    }
    [[NSUserDefaults standardUserDefaults] setInteger:[folderCheckbox state] forKey:@"MoveFolderMode"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    

}


@end
