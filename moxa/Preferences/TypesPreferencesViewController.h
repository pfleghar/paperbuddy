//
// This is a sample General preference pane
//

#import "MASPreferencesViewController.h"

@interface TypesPreferencesViewController : NSViewController <MASPreferencesViewController>

@property (strong) IBOutlet NSPopUpButton *notebookPopup;
@property (weak) IBOutlet NSTokenField *tagsField;
@property (weak) IBOutlet NSTableView *tableView;
@property (weak) IBOutlet NSTokenField *keywordsField;
@property (strong) IBOutlet NSButton *removeTypeButton;
@property (strong) IBOutlet NSButton *addTypeButton;
@property (strong) IBOutlet NSTextField *titleField;

- (IBAction)changeCurrentType:(id)sender;
- (IBAction)addType:(id)sender;
- (IBAction)removeType:(id)sender;
- (IBAction)notebookChanged:(id)sender;

@end
