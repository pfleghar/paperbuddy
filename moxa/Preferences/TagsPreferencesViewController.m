
#import "TagsPreferencesViewController.h"

@implementation TagsPreferencesViewController

@synthesize tokenField;
@synthesize useGeneralTagsCheckbox;

- (id)init
{
    return [super initWithNibName:@"TagsPreferencesView" bundle:nil];
}

#pragma mark -
#pragma mark MASPreferencesViewController

- (NSString *)identifier
{
    return @"PeoplePreferences";
}

- (NSImage *)toolbarItemImage
{
    return [NSImage imageNamed:@"15-tags"];

}

- (NSString *)toolbarItemLabel
{
    return NSLocalizedString(@"Tags", @"Toolbar item name for the People preference pane");
}



- (void)awakeFromNib
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"GeneralTags"]) {
        [tokenField setStringValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"GeneralTags"]];
    }
    [useGeneralTagsCheckbox setState:[[NSUserDefaults standardUserDefaults] boolForKey:@"UseGeneralTags"]];
    [tokenField setEnabled:(long)[useGeneralTagsCheckbox state]];

}


- (void)controlTextDidChange:(NSNotification *)aNotification {
    [[NSUserDefaults standardUserDefaults] setObject:[tokenField stringValue] forKey:@"GeneralTags"];
    [[NSUserDefaults standardUserDefaults] synchronize];

}

- (IBAction)useGeneralTagsAction:(id)sender {
    [tokenField setEnabled:(long)[useGeneralTagsCheckbox state]];
    [[NSUserDefaults standardUserDefaults] setBool: [useGeneralTagsCheckbox state] forKey:@"UseGeneralTags"];
    [[NSUserDefaults standardUserDefaults] synchronize];
   
}
@end
