//
// This is a sample General preference pane
//

#import "MASPreferencesViewController.h"

@interface GeneralPreferencesViewController : NSViewController <MASPreferencesViewController>

@property (strong) IBOutlet NSPathControl *folderPathControl;
@property (strong) IBOutlet NSButton *folderCheckbox;

- (IBAction)chooseFolder:(id)sender;
- (IBAction)moveChanged:(id)sender;
- (IBAction)logoutFromEvernote:(id)sender;

@end
