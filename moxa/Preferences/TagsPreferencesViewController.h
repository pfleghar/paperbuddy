//
// This is a sample General preference pane
//

#import "MASPreferencesViewController.h"

@interface TagsPreferencesViewController : NSViewController <MASPreferencesViewController, NSTokenFieldDelegate>
@property (strong) IBOutlet NSTokenField *tokenField;
@property (weak) IBOutlet NSButton *useGeneralTagsCheckbox;
- (IBAction)useGeneralTagsAction:(id)sender;

@end
