//
//  PBDropView.m
//  PaperBuddy
//
//  Created by Ralf Pfleghar on 27.02.13.
//  Copyright (c) 2013 Ralf Pfleghar. All rights reserved.
//

#import "PBDropView.h"

@implementation PBDropView
NSImage *bgImage;

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    
    [self registerForDraggedTypes: [NSArray arrayWithObjects:NSPDFPboardType,NSFilenamesPboardType,nil]];
    
    return self;
}


- (void)drawRect:(NSRect)dirtyRect
{
    NSRect bounds = [self bounds];
    NSSize imageSize = [bgImage size];
    
    // start at max Y (top) so that resizing the window looks to be anchored at the top left
    for ( float y = NSHeight(bounds) - imageSize.height; y >= -imageSize.height; y -= imageSize.height ) {
        for ( float x = NSMinX(bounds); x < NSWidth(bounds); x += imageSize.width ) {
            NSRect tileRect = NSMakeRect(x, y, imageSize.width, imageSize.height);
            if ( NSIntersectsRect(tileRect, dirtyRect) ) {
                NSRect destRect = NSIntersectionRect(tileRect, dirtyRect);
                [bgImage drawInRect:destRect
                           fromRect:NSOffsetRect(destRect, -x, -y)
                          operation:NSCompositeSourceOver
                           fraction:1.0];
            }
        }
    }
}

- (void)awakeFromNib
{
    bgImage = [[NSImage alloc] initWithContentsOfFile: [[NSBundle mainBundle] pathForResource:@"exclusive_paper" ofType:@"png"]];
    [self setNeedsDisplay:YES];
}




- (NSDragOperation)draggingEntered:(id )sender
{
    if ((NSDragOperationGeneric & [sender draggingSourceOperationMask])
        == NSDragOperationGeneric) {
        
        return NSDragOperationGeneric;
        
    } // end if
    
    // not a drag we can use
    return NSDragOperationNone;
    
} // end draggingEntered

- (BOOL)prepareForDragOperation:(id )sender {
    return YES;
} // end prepareForDragOperation




- (BOOL)performDragOperation:(id )sender {
    NSPasteboard *zPasteboard = [sender draggingPasteboard];
    // define the images  types we accept
    // NSPasteboardTypeTIFF: (used to be NSTIFFPboardType).
    // NSFilenamesPboardType:An array of NSString filenames
    NSArray *zImageTypesAry = [NSArray arrayWithObjects:NSPasteboardTypePDF,  NSFilenamesPboardType, nil];
    NSString *zDesiredType = [zPasteboard availableTypeFromArray:zImageTypesAry];
    
    if ([zDesiredType isEqualToString:NSPasteboardTypePDF]) {
        DebugLog(@"PDF Dropped");
        return YES;
    } //end if
    
    
    if ([zDesiredType isEqualToString:NSFilenamesPboardType]) {
        // the pasteboard contains a list of file names
        //Take the first one
//        NSArray *zFileNamesAry = [zPasteboard propertyListForType:@"NSFilenamesPboardType"];
//        NSString *zPath = [zFileNamesAry objectAtIndex:0];
        DebugLog(@"Multiple Dropped");
        
        return YES;
    }
         
    //this cant happen ???
    DebugLog(@"Error MyNSView performDragOperation");
    return NO;
    
} // end performDragOperation


- (void)concludeDragOperation:(id )sender {
    [self setNeedsDisplay:YES];
} // end concludeDragOperation



@end
