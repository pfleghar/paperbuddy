//
//  PBPDFDocument.h
//  PaperBuddy
//
//  Created by Ralf Pfleghar on 27.02.13.
//  Copyright (c) 2013 Ralf Pfleghar. All rights reserved.
//

#import <PDFKit/PDFKit.h>

@interface PBPDFDocument : PDFDocument

@property  (copy) NSString *documentTitle;
@property  NSString *documentTags;
@property  NSString *documentNotebook;
@property  NSDate *documentDate;
@property  NSDictionary *documentType;
@property  NSURL *documentUrl;

- (void) applyDocumentType: (BOOL) forceTitle;
- (NSMutableArray *) documentTagsAsArray;

@end
