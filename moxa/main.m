//
//  main.m
//  PaperBuddy
//
//  Created by Ralf Pfleghar on 25.02.13.
//  Copyright (c) 2013 Ralf Pfleghar. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
