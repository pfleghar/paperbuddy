//
//  PBAppDelegate.h
//  PaperBuddy
//
//  Created by Ralf Pfleghar on 25.02.13.
//  Copyright (c) 2013 Ralf Pfleghar. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <PDFKit/PDFKit.h>
#import "PBPDFDocument.h"
#import "PBDocumentTypeController.h"
#import "PBMainView.h"

@interface PBAppDelegate : NSObject <NSApplicationDelegate,NSTokenFieldDelegate, NSTableViewDelegate>

@property NSMutableArray    *openFiles;

@property PBPDFDocument     *currentFile;
@property PBPDFDocument     *sentFile;

@property PBDocumentTypeController    *documentTypeController;
@property int hasBeenStartedWithDocument;

@property (nonatomic) NSWindowController *preferencesWindowController;
@property (nonatomic) NSInteger focusedAdvancedControlIndex;

@property (weak) IBOutlet PBMainView *mainView;
@property (assign) IBOutlet NSWindow *window;
@property (weak) IBOutlet PDFView *pdfView;

@property (weak) IBOutlet NSScrollView *documentTypeScrollView;
@property (weak) IBOutlet NSTableView *documentTypeTable;

@property (weak) IBOutlet NSTextField *documentTitleField;
@property (weak) IBOutlet NSBox *titleLine;

@property (weak) IBOutlet NSDatePicker *documentDateField;
@property (weak) IBOutlet NSTextField *documentLabel;

@property (weak) IBOutlet NSTextField *tagsLabel;
@property (weak) IBOutlet NSTokenField *documentTagsField;

@property (weak) IBOutlet NSPopUpButton *notebookPopup;
@property (weak) IBOutlet NSTextField *notebookLabel;

@property (weak) IBOutlet NSTextField *evernoteLabel;
@property (weak) IBOutlet NSButton *evernoteButton;
@property (weak) IBOutlet NSProgressIndicator *evernoteSpinner;
@property (weak) IBOutlet NSTokenField *tagsTokenField;

- (IBAction)discardCurrentDocument:(id)sender;

- (IBAction)sendToEvernote:(id)sender;
- (IBAction)nextDocument:(id)sender;
- (IBAction)previousDocument:(id)sender;
- (IBAction)dateChanged:(id)sender;
- (IBAction)notebookPopupChanged:(id)sender;
- (IBAction)viewDocument:(id)sender;
- (IBAction)openDocument:(id)sender;



@end

