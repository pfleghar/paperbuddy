//
//  PBDocumentTypes.m
//  PaperBuddy
//
//  Created by Ralf Pfleghar on 01.03.13.
//  Copyright (c) 2013 Ralf Pfleghar. All rights reserved.
//

#import "PBDocumentTypeController.h"
#import "PBEvernoteController.h"


@implementation PBDocumentTypeController

@synthesize currentType;
@synthesize types;

+ (PBDocumentTypeController *)sharedInstance
{
    static PBDocumentTypeController * instance = nil;
    
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        // --- call to super avoids a deadlock with the above allocWithZone
        instance = [[super allocWithZone:nil] init];
    });
    
    return instance;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        currentType = nil;
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"DocumentTypes"] == nil) {
            NSMutableArray *def = [[NSMutableArray alloc] init];
            [self setTypes:def];
        } else {
            NSMutableArray *sane = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"DocumentTypes"]];
            
            // checksanity
            for (NSMutableDictionary *object in sane) {
                
                if ([object objectForKey:@"Title"] == nil) {
                    [object setObject:@"" forKey:@"Title"];
                }
                if ([object objectForKey:@"Tags"] == nil) {
                    [object setObject:@"" forKey:@"Tags"];
                }
                if ([object objectForKey:@"Keywords"] == nil) {
                    [object setObject:@"" forKey:@"Keywords"];
                }
                if ([object objectForKey:@"Notebook"] == nil) {
                    [object setObject:@"" forKey:@"Notebook"];
                }

            }
            
            [self setTypes:sane];
        
        }

    }
    return self;
}

- (void) synchronizeTypes {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"DocumentTypes"];
    [[NSUserDefaults standardUserDefaults] setObject:[self types] forKey:@"DocumentTypes"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DocumentTypesHaveChanged" object:self];
}

- (NSMutableArray *) types{
    return types;

}

- (void) setTypes: (NSMutableArray *) _types{
    types = _types;
    [self synchronizeTypes];
    
}

- (NSInteger *) indexOfCurrentType {
    return (long *)[[self types] indexOfObject:currentType];
}

- (void) setCurrentTypeByNumber: (NSInteger) number {
    if (number > [[self types] count] || number < 0 ) {
        currentType = nil;
    } else {
        currentType = [[self types] objectAtIndex:number];
    }
}

- (void) updateCurrentType: (NSMutableDictionary *) newType  {
    [types replaceObjectAtIndex:[[self types] indexOfObject:currentType] withObject:newType];
    [self setCurrentType:newType];
}


- (void) addType {
    NSMutableDictionary *new = [[NSMutableDictionary alloc] init];
    [new setObject:@"New Type" forKey:@"Name"];
    [new setObject:[[PBEvernoteController sharedInstance] defaultNotebook] forKey:@"Notebook"];
    [new setObject:[[NSArray alloc] init] forKey:@"Tags"];
    [new setObject:[[NSArray alloc] init] forKey:@"Keywords"];
    [new setObject:@"" forKey:@"Title"];

    [types addObject:new];
    [self setCurrentType:new];
    [self synchronizeTypes];
}


#pragma mark - Table Data Source
- (NSInteger)numberOfRowsInTableView:(NSTableView *)aTableView {
    NSInteger i = [[self types] count];
    return i;
}

- (id)tableView:(NSTableView *)aTableView objectValueForTableColumn:(NSTableColumn *)aTableColumn row:(NSInteger)rowIndex {
    NSMutableDictionary *dict = [[self types] objectAtIndex:rowIndex];
    return [dict objectForKey:@"Name"];
}

- (void)tableView:(NSTableView *)aTableView sedtObjectValue:(id)anObject forTableColumn:(NSTableColumn *)aTableColumn row:(NSInteger)rowIndex {
    NSMutableDictionary *dict = [[self types] objectAtIndex:rowIndex];
    [dict setObject:anObject forKey:@"Name"];
    [self synchronizeTypes];
    
}


- (BOOL)tableView:(NSTableView *)aTableView acceptDrop:(id < NSDraggingInfo >)info row:(NSInteger)row dropOperation:(NSTableViewDropOperation)operation {
    NSPasteboard* pboard = [info draggingPasteboard];
    NSData* rowData = [pboard dataForType:moxaDocumentTypeTableData];
    NSIndexSet* rowIndexes = [NSKeyedUnarchiver unarchiveObjectWithData:rowData];
    NSInteger dragRow = [rowIndexes firstIndex];
    
    // Move the specified row to its new location...
    // if we remove a row then everything moves down by one
    // so do an insert prior to the delete
    // --- depends which way we're moving the data!!!
    if (dragRow < row) {
        [types insertObject: [types objectAtIndex:dragRow] atIndex:row];
        [types removeObjectAtIndex:dragRow];
        [aTableView noteNumberOfRowsChanged];
        [aTableView reloadData];
        [self synchronizeTypes];

        return YES;
        
    } // end if
    
    NSMutableDictionary *zData = [types objectAtIndex:dragRow];
    [types removeObjectAtIndex:dragRow];
    [types insertObject:zData atIndex:row];
    [aTableView noteNumberOfRowsChanged];
    [aTableView reloadData];
    [self synchronizeTypes];

    return YES;
}

- (NSDragOperation)tableView:(NSTableView *)aTableView validateDrop:(id < NSDraggingInfo >)info proposedRow:(NSInteger)row proposedDropOperation:(NSTableViewDropOperation)operation {
    if (operation == NSTableViewDropAbove) {
        return NSDragOperationMove;
    } else {
        return NSDragOperationNone;
    }
}

- (BOOL)tableView:(NSTableView *)aTableView writeRowsWithIndexes:(NSIndexSet *)rowIndexes toPasteboard:(NSPasteboard *)pboard {
    // Copy the row numbers to the pasteboard.
    NSData *zNSIndexSetData = [NSKeyedArchiver archivedDataWithRootObject:rowIndexes];
    [pboard declareTypes:[NSArray arrayWithObject:moxaDocumentTypeTableData]owner:self];
    [pboard setData:zNSIndexSetData forType:moxaDocumentTypeTableData];
    return YES;
}


@end
