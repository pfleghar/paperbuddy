//
//  PBEvernoteController.m
//  PaperBuddy
//
//  Created by Ralf Pfleghar on 11.03.13.
//  Copyright (c) 2013 Ralf Pfleghar. All rights reserved.
//

#import "PBEvernoteController.h"
#import "EvernoteSDK.h"



@implementation PBEvernoteController

+ (PBEvernoteController *)sharedInstance
{
    static PBEvernoteController * instance = nil;
    
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        // --- call to super avoids a deadlock with the above allocWithZone
        instance = [[super allocWithZone:nil] init];
    });

    
    return instance;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        
    }
    return self;
}

- (NSArray *) tagsAsStrings {
    
    NSMutableArray *strings = [[NSMutableArray alloc] init];
    for (EDAMTag *t in [self tags]) {
        [strings addObject:[t name]];
    }
    return strings;
}

- (BOOL) hasTagWithName: (NSString *) tag {
    for (EDAMTag *t in [self tags]) {
        if ([[t name] isEqualToString:tag]) {
            return true;
        }
    }
    return false;
}

- (NSString *) guidForName: (NSString *) name {
    NSArray *notebookNames = [self notebooks];
    for (int i = 0; i< [notebookNames count] ; i++){
        NSString *n = [[notebookNames objectAtIndex:i] name];
        if ([n isEqualToString:name]) {
            return [[notebookNames objectAtIndex:i] guid];
        }
    }
    return @"";
}

- (BOOL) hasNotebookWithName: (NSString *) name {
    NSArray *notebookNames = [self notebooks];
    for (int i = 0; i< [notebookNames count] ; i++){
        NSString *n = [[notebookNames objectAtIndex:i] name];
        if ([n isEqualToString:name]) {
            return YES;
        }
    }
    return NO;
    
}

- (NSString *) defaultNotebook {
    NSMutableArray *books = [self notebooks];
    
    for (EDAMNotebook *book in books) {
        if ([book defaultNotebook]) {
            return [book name];
        }
    }
    return @"";
    
}

- (NSArray *) notebookNames {
    NSMutableArray *names = [[NSMutableArray alloc] init];
    NSMutableArray *books = [self notebooks];
    
    for (EDAMNotebook *book in books) {
        if ([book defaultNotebook]) {
            [names insertObject:[book name] atIndex:0];
        } else {
        [names addObject:[book name]];
        }
    }
    return names;
}

- (void) synchronizeWithEvernote:(id)sender
{
    // set up Evernote session singleton
    NSString *authPath = [[NSBundle mainBundle] pathForResource:@"evernote-auth.plist" ofType:nil];
    NSDictionary *consumerInfo = [NSDictionary dictionaryWithContentsOfFile:authPath];

    [EvernoteSession setSharedSessionHost:BootstrapServerBaseURLStringUS
    //[EvernoteSession setSharedSessionHost:BootstrapServerBaseURLStringSandbox

                              consumerKey:consumerInfo[kConsumerAPIKeyKey]
                           consumerSecret:consumerInfo[kConsumerAPISecretKey]];
    
    // no-op view controller to satisfy the iOS portion of the SDK
    NSViewController *viewController = [NSViewController new];
    
    EvernoteSession *session = [EvernoteSession sharedSession];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"EvernoteSynchingStarted" object:self];

    [session authenticateWithViewController:viewController completionHandler:^(NSError *error) {
        
        if (error || !session.isAuthenticated)
        {
            DebugLog(@"NOT authenticated: %@", error);
            NSRunAlertPanel(@"Evernote access denied", @"This App needs access to your Evernote account. Please try again.", @"OK", nil, nil);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"EvernoteSynchingFailed" object:self];
            [session logout];
        }
        else
        {
            DebugLog(@"authenticated! noteStoreUrl:%@ webApiUrlPrefix:%@", session.noteStoreUrl, session.webApiUrlPrefix);
            
            DebugLog(@"fetching note information...");
            
            
            // grab the notebooks
            [[EvernoteNoteStore noteStore] listNotebooksWithSuccess:^(NSArray *notebooks) {
                // store the notebooks
                self.notebooks = [NSMutableArray arrayWithArray:notebooks];
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:@"EvernoteSynchingFinished" object:self];
            } failure:^(NSError *error) {
                DebugLog(@"error %@", error);
            }];
            
            // grab the tags
            [[EvernoteNoteStore noteStore] listTagsWithSuccess:^(NSArray *entags) {
                // store the tags
                self.tags = [NSArray arrayWithArray:entags];
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:@"EvernoteSynchingFinished" object:self];
            } failure:^(NSError *error) {
                DebugLog(@"error %@", error);
            }];


            
        }
    }];
    
}


@end
