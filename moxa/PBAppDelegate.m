//
//  PBAppDelegate.m
//  PaperBuddy
//
//  Created by Ralf Pfleghar on 25.02.13.
//  Copyright (c) 2013 Ralf Pfleghar. All rights reserved.
//

#import "PBAppDelegate.h"
#import <PDFKit/PDFKit.h>
#import "MASPreferencesWindowController.h"
#import "GeneralPreferencesViewController.h"
#import "TagsPreferencesViewController.h"
#import "TypesPreferencesViewController.h"
#import "PBEvernoteController.h"
#import "EvernoteSDK.h"
#import "EDAM.h"



@implementation PBAppDelegate

@synthesize documentTitleField;

@synthesize documentDateField;
@synthesize openFiles;
@synthesize currentFile;
@synthesize sentFile;
@synthesize documentTypeController;
@synthesize documentTypeTable;
@synthesize documentTypeScrollView;
@synthesize documentTagsField;
@synthesize notebookPopup;
@synthesize documentLabel;
@synthesize hasBeenStartedWithDocument;
@synthesize evernoteButton;

#pragma mark - Entry Shit
- (void)applicationWillFinishLaunching:(NSNotification *)aNotification {
    DebugLog(@"applicationWillFinishLaunching");
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    DebugLog(@"applicationDidFinishLaunching");
    self.hasBeenStartedWithDocument = 1;
}


- (BOOL)application:(NSApplication *)theApplication openFile:(NSString *)filename {
    
    DebugLog(@"application");
    self.hasBeenStartedWithDocument = 0;
    
    NSURL *fileUrl = [NSURL fileURLWithPath:filename];
    
    PBPDFDocument *pdfDocument = [[PBPDFDocument alloc] initWithURL:fileUrl];
    if (pdfDocument == nil){
        return false;
    }
    
    [openFiles addObject:pdfDocument];
    currentFile = pdfDocument;
    sentFile = nil;
    [self updateFromDocument];
    
    return true;
}




- (void)awakeFromNib
{
    DebugLog(@"awakeFromNib");
    self.hasBeenStartedWithDocument = -1;
    self.openFiles = [[NSMutableArray alloc] init];
    
    self.documentTypeController = [PBDocumentTypeController sharedInstance];
    [documentTypeTable setDataSource: documentTypeController];
    [documentDateField setEnabled:false];
    [documentTitleField setEnabled:false];
    [documentTypeTable setEnabled:false];
    [documentTagsField setEnabled:false];
    [notebookPopup setEnabled:false];
    [evernoteButton setEnabled:false];
    
    [[self pdfView] setBackgroundColor: [NSColor whiteColor]];
    [self updateFromDocument];
    [[self window] center];
    
    [[PBEvernoteController sharedInstance] synchronizeWithEvernote:self];

    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(evernoteSendingFinished:)
                                                 name:@"EvernoteSendingFinished" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(evernoteSendingStarted:)
                                                 name:@"EvernoteSendingStarted" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(evernoteSynchingFinished:)
                                                 name:@"EvernoteSynchingFinished" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(evernoteSynchingStarted:)
                                                 name:@"EvernoteSynchingStarted" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(documentTypesHaveChanged:)
                                                 name:@"DocumentTypesHaveChanged" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(evernoteSynchingFailed:)
                                                 name:@"EvernoteSynchingFailed" object:nil];
    
    
    // not sure why I have to do this, did in IB
    [[self tagsTokenField] setDelegate:self];
    
    
    [NSApp activateIgnoringOtherApps:YES];
}


#pragma mark - Notifications

- (void)documentTypesHaveChanged:(NSNotification *)notification {
    [currentFile applyDocumentType: false];
    [self updateUI];
}

- (void)evernoteSynchingFailed:(NSNotification *)notification {
    //exit(0);
    
    EvernoteSession *session = [EvernoteSession sharedSession];
    [session logout];

    [[PBEvernoteController sharedInstance] synchronizeWithEvernote:self];

}

- (void)evernoteSynchingFinished:(NSNotification *)notification {
    [currentFile setDocumentNotebook:[[PBEvernoteController sharedInstance] defaultNotebook]];
    [[self evernoteLabel] setHidden:YES];
    [[self evernoteSpinner] setHidden:YES];
    [[self evernoteSpinner] stopAnimation:self];
    [self updateUI];
}

- (void)evernoteSynchingStarted:(NSNotification *)notification {
    [[self evernoteLabel] setHidden:NO];
    [[self evernoteSpinner] setHidden:NO];
    [[self evernoteSpinner] startAnimation:self];
    [self updateUI];
}

- (void)evernoteSendingFinished:(NSNotification *)notification {
    [[self evernoteLabel] setHidden:YES];
    [[self evernoteSpinner] setHidden:YES];
    [[self evernoteSpinner] stopAnimation:self];
    [[self evernoteButton] setEnabled:true];
    [self updateUI];

#warning this should be prettier
    for (NSDictionary *app in [[NSWorkspace sharedWorkspace] launchedApplications])
    {
        if ([@"com.evernote.Evernote" isEqualToString:[app objectForKey:@"NSApplicationBundleIdentifier"]])
        {
            ProcessSerialNumber psn;
            GetCurrentProcess(&psn); // Initialize the Process Manager
            psn.highLongOfPSN = [[app objectForKey:@"NSApplicationProcessSerialNumberHigh"] intValue];
            psn.lowLongOfPSN = [[app objectForKey:@"NSApplicationProcessSerialNumberLow"] intValue];
            ShowHideProcess(&psn, NO);
        }
    }
}

- (void)evernoteSendingStarted:(NSNotification *)notification {
    [[self evernoteLabel] setHidden:NO];
    [[self evernoteSpinner] setHidden:NO];
    [[self evernoteSpinner] startAnimation:self];
    [[self evernoteButton] setEnabled:false];

    [self updateUI];
}


#pragma mark - Public accessors

- (NSWindowController *)preferencesWindowController
{
    if (_preferencesWindowController == nil)
    {
        NSViewController *generalViewController = [[GeneralPreferencesViewController alloc] init];
        NSViewController *peopleViewController = [[TagsPreferencesViewController alloc] init];
        NSViewController *typesViewController = [[TypesPreferencesViewController alloc] init];
        
        NSArray *controllers = [[NSArray alloc] initWithObjects:generalViewController, typesViewController, peopleViewController, nil];
        
        // To add a flexible space between General and Advanced preference panes insert [NSNull null]:
        //     NSArray *controllers = [[NSArray alloc] initWithObjects:generalViewController, [NSNull null], advancedViewController, nil];
        
        
        NSString *title = NSLocalizedString(@"Preferences", @"Common title for Preferences window");
        _preferencesWindowController = [[MASPreferencesWindowController alloc] initWithViewControllers:controllers title:title];
    }
    return _preferencesWindowController;
}


- (void) updateUI {
    
    [documentTypeTable reloadData];
    [documentDateField setEnabled:([self currentFile] != nil)];
    [documentTitleField setEnabled:([self currentFile] != nil)];
    [documentTypeTable setEnabled:([self currentFile] != nil)];
    [documentTagsField setEnabled:([self currentFile] != nil)];
    [notebookPopup setEnabled:([self currentFile] != nil)];
    [evernoteButton setEnabled:([self currentFile] != nil && sentFile == nil)];
    
    
    [[self notebookPopup] removeAllItems];
    [[self notebookPopup] addItemsWithTitles:[[PBEvernoteController sharedInstance] notebookNames]];
    if ([[PBEvernoteController sharedInstance] hasNotebookWithName:[[self currentFile] documentNotebook]]) {
        [[self notebookPopup] selectItemWithTitle:[[self currentFile] documentNotebook]];
    } else {
        [[self notebookPopup] selectItemWithTitle:[[PBEvernoteController sharedInstance] defaultNotebook]];
        
    }
    
    long hit = [openFiles indexOfObject:currentFile];
    [[self documentLabel] setStringValue:[NSString stringWithFormat:@"Document %d/%d", (int) hit+1, (int)[openFiles count]] ];
    
}


- (void) updateFromDocument {
    
    
//    [currentFile applyDocumentType];
//    [currentFile determineTitle];
    
    
    // set picture
    [[self pdfView] setDocument:currentFile];
    
    // get document info
    if ([currentFile documentTitle]) {
        [documentTitleField setStringValue: [currentFile documentTitle]];
    } else {
        [documentTitleField setStringValue: @""];
        
    }
    
    [documentDateField setDateValue: [currentFile documentDate]];
    
    if ([currentFile documentNotebook]) {
        [notebookPopup selectItemWithTitle: [currentFile documentNotebook]];
    } else {
        [notebookPopup selectItemWithTitle:[[PBEvernoteController sharedInstance] defaultNotebook]];
    }
    
    NSUInteger index = [[[PBDocumentTypeController sharedInstance] types] indexOfObject:[currentFile documentType]];
    
    if (index != NSNotFound) {
        NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:index];
        [documentTypeTable selectRowIndexes:indexSet byExtendingSelection:NO];
    } else {
        [documentTypeTable deselectAll:self];
    }
    
    if ([currentFile documentTags]) {
        [documentTagsField setStringValue: [currentFile documentTags]];
    } else {
        [documentTagsField setStringValue:@""];
    }
    
    
    [self updateUI];
    
}



#pragma mark - Actions

- (IBAction)openPreferences:(id)sender {
    [self.preferencesWindowController showWindow:nil];
    
}


- (void)hideCurrentDocument:(id)sender {
    
    sentFile = currentFile;
    [self discardCurrentDocument:self];
}


- (IBAction)discardCurrentDocument:(id)sender {
    
    PBPDFDocument *remove = currentFile;
    [self previousDocument:self];
    [openFiles removeObjectIdenticalTo:remove];
    if ([openFiles count]>0) {
        currentFile = [openFiles objectAtIndex:0];
    } else {
        currentFile = nil;
        if ((self.hasBeenStartedWithDocument) &&(sentFile == nil)) {
            [[NSApplication sharedApplication] terminate:self];
        }
    
    }
    
    
    [self updateFromDocument];
    
    // bring app to front
    [[NSRunningApplication currentApplication] activateWithOptions:NSApplicationActivateIgnoringOtherApps] ;
    
    
}

- (IBAction)sendToEvernote:(id)sender {
    NSMutableString *titleString;
    
    [[self evernoteButton] setEnabled:false];
    [self hideCurrentDocument: self];
    
    
    if (![sentFile documentTitle] || [[sentFile documentTitle] isEqualToString:@""]) {
        titleString = [[NSMutableString alloc] initWithString:@"Untitled"];
    } else {
        titleString = [[NSMutableString alloc] initWithString:[sentFile documentTitle]];
    }
    
#ifdef DEBUG
    [titleString appendString:@" [DEBUG]"];
#endif
   
    
// https://github.com/ngs/evernote-sdk-mac/blob/master/evernote-sdk-mac/evernote-sdk-mac/EDAMNote%2BAttachment.m
    [[NSNotificationCenter defaultCenter] postNotificationName:@"EvernoteSendingStarted" object:self];

    EDAMNote *note = [[EDAMNote alloc] initWithAttachmentURL:[sentFile documentUrl]];
    [note setCreated:[sentFile documentDate].edamTimestamp];
    
    // title
    [note setTitle: [titleString stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]]];

    // tags
    if ([sentFile documentTags] && [[sentFile documentTags] length] > 0) {
        DebugLog(@"Tags: %@", [[NSMutableArray alloc] initWithArray:[[sentFile documentTags] componentsSeparatedByString:@","]]);
        [note setTagNames:[sentFile documentTagsAsArray]];
    }

    // notebook
    if (![[[PBEvernoteController sharedInstance] guidForName:[sentFile documentNotebook]] isEqualToString:@""]) {
        [note setNotebookGuid: [[PBEvernoteController sharedInstance] guidForName:[sentFile documentNotebook]]];
    }

   
    [[EvernoteNoteStore noteStore]
     createNote:note
     success:^(EDAMNote *note) {
         
         // We have to request EDAMUser because user had shardId and userId
         [[EvernoteUserStore userStore]
          getUserWithSuccess:^(EDAMUser *user) {
              
              NSURL *webURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.evernote.com/view/notebook/%@", note.guid]];
              NSURL *clientURL = [NSURL URLWithString:[NSString stringWithFormat:@"evernote:///view/%d/%@/%@//",
                                                       user.id, user.shardId, note.guid]];
              
              DebugLog(@"clientUrl: %@", clientURL);
              // Note Links are structed with user id, shard id, note guid
              // See http://dev.evernote.com/documentation/cloud/chapters/Note_Links.php
              
#warning make this a user choice
              
              NSWorkspace *ws = [NSWorkspace sharedWorkspace];
              if(![ws openURL:clientURL]) { // If app URI does not be handled
                  [ws openURL:webURL]; //      Opens Evernote web instead.
              }

              
              [self moveDocument];

              // clear file and enable sending again
              sentFile = nil;
              [[self evernoteButton] setEnabled:true];

              
              
              [[NSNotificationCenter defaultCenter]
               postNotificationName:@"EvernoteSendingFinished" object:self];

              
          }
          failure:^(NSError *error) {
              [[self evernoteButton] setEnabled:true];
              [[NSNotificationCenter defaultCenter]
               postNotificationName:@"EvernoteSendingFinished" object:self];

              [[NSAlert alertWithError:error] runModal];
          }];
     }
     failure:^(NSError *error) {
         [[self evernoteButton] setEnabled:true];
         [[NSNotificationCenter defaultCenter]
          postNotificationName:@"EvernoteSendingFinished" object:self];

         [[NSAlert alertWithError:error] runModal];
     }];
}



//- (IBAction)sendToEvernoteAS:(id)sender {
//    NSDictionary *errorInfo;
//    
//    // start Evernote we will be needing it
//    [[NSWorkspace sharedWorkspace] launchAppWithBundleIdentifier:@"com.evernote.Evernote"
//                                                         options:(NSWorkspaceLaunchWithoutActivation |
//                                                                  NSWorkspaceLaunchAllowingClassicStartup)
//                                  additionalEventParamDescriptor:nil launchIdentifier:NULL];
//    wait(1000);
//    // prepare tags
//    NSArray *tags = [[currentFile documentTags] componentsSeparatedByString:@","];
//    NSMutableString *preparedTags = [[NSMutableString alloc] init];
//    for (int i=0; i< [tags count]; i++) {
//        if ([tags objectAtIndex:i]!= nil && ![[tags objectAtIndex:i] isEqualToString:@""]) {
//            [preparedTags appendString:[NSMutableString stringWithFormat:@"\"%@\",", [tags objectAtIndex:i]]];
//        }
//    }
//    if ( [preparedTags length] > 0)
//        preparedTags = (NSMutableString *)[preparedTags substringToIndex:[preparedTags length] - 1];
//    
//    // prepare date
//    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    [formatter setDateStyle:NSDateFormatterLongStyle]; // z.B. 08.10.2008
//    [formatter setTimeStyle:NSDateFormatterNoStyle]; // keine Zeitangabe
//    
//    DebugLog(@"Formatted date: %@", [formatter stringFromDate:[currentFile documentDate]]);
//    NSMutableString *titleString = [[NSMutableString alloc] initWithString:[currentFile documentTitle]];
//    
//#ifdef DEBUG
//    [titleString appendString:@" [DEBUG]"];
//#endif
//    
//    NSString *appleScript = [NSString stringWithFormat:@"tell application id \"com.evernote.Evernote\"\n activate\n create note from url \"%@\" title \"%@\" tags {%@} notebook \"%@\" created date \"%@\" \nend tell\n",[currentFile documentUrl], titleString, preparedTags, [currentFile documentNotebook], [formatter stringFromDate:[currentFile documentDate]]];
//    DebugLog(@"AppleScript: %@",appleScript);
//    
//	NSAppleScript *run = [[NSAppleScript alloc] initWithSource:appleScript];
//	[run executeAndReturnError:&errorInfo];
//    
//    
//    
//    // if worked, renove doc and do next one
//    if (errorInfo) {
//        /* create the error message string */
//        NSString *err = [NSString stringWithFormat:
//                         @"Error %@ occured the %@(%@) call: %@",
//                         [errorInfo objectForKey:NSAppleScriptErrorNumber],
//                         @"", @"",
//                         [errorInfo objectForKey:NSAppleScriptErrorBriefMessage]];
//        DebugLog(@"%@",err);
//        [self displaError:@"Could not send file to Evernote" informativeText:[errorInfo objectForKey:NSAppleScriptErrorBriefMessage]];
//        
//        
//    } else {
//        [self moveDocument];
//        [self discardCurrentDocument:self];
//    }
//    
//}
- (void) displaError: (NSString *) messageText informativeText:(NSString *) informativeText {
    
    NSAlert *alert = [[NSAlert alloc] init];
    [alert addButtonWithTitle:@"OK"];
    [alert setMessageText:messageText];
    [alert setInformativeText:informativeText];
    [alert setAlertStyle:NSWarningAlertStyle];
    [alert beginSheetModalForWindow:[self window] modalDelegate:self didEndSelector:@selector(alertDidEnd:returnCode:contextInfo:) contextInfo:nil];
    
}

- (void)alertDidEnd:(NSAlert *)alert returnCode:(NSInteger)returnCode contextInfo:(void *)contextInfo {
    if (returnCode == NSAlertFirstButtonReturn) {
        DebugLog(@"Response");
    }
}


- (void) moveDocument {
    NSError *error = nil;
    BOOL bookmarkDataIsStale;
    NSURL *bookmarkFileURL = nil;

    
    NSInteger i = [[NSUserDefaults standardUserDefaults] integerForKey:@"MoveFolderMode"];
    
    NSData *moveToData = [[NSUserDefaults standardUserDefaults] objectForKey:@"MoveFolderData"];
  
    bookmarkFileURL = [NSURL
                       URLByResolvingBookmarkData:moveToData
                       options:NSURLBookmarkResolutionWithSecurityScope
                       relativeToURL:nil
                       bookmarkDataIsStale:&bookmarkDataIsStale
                       error:&error];
    
    NSString *file = [[sentFile documentUrl] lastPathComponent];
    
    [bookmarkFileURL startAccessingSecurityScopedResource];
    
    DebugLog(@"About to move: \"%@\" to \"%@\"",[sentFile documentUrl], [bookmarkFileURL URLByAppendingPathComponent:file] );

#ifdef DEBUG
    i = 0;
#endif
    
    // check if user wants to do this
    if (i==1) {
        NSError *error = nil;
        if (![[NSFileManager defaultManager] moveItemAtURL:[sentFile documentUrl] toURL:[bookmarkFileURL URLByAppendingPathComponent:file] error:&error])
        {
            [self displaError:@"Cound not move file" informativeText:[error localizedDescription]];
        }
        
    } else {
        
        DebugLog(@"Not moving because user didn't want that");
        
    }
    
    [bookmarkFileURL stopAccessingSecurityScopedResource];
    
    sentFile = nil;
    
    if ((self.hasBeenStartedWithDocument) && (sentFile == nil) && ([openFiles count] ==0)) {
        [[NSApplication sharedApplication] terminate:self];
    }
    
}

- (IBAction)nextDocument:(id)sender {
    long hit = [openFiles indexOfObject:currentFile];
    
    if (hit < ([openFiles count]-1)) {
        hit++;
    }
    
    currentFile = [openFiles objectAtIndex:hit];
    
    [self updateFromDocument];
}

- (IBAction)previousDocument:(id)sender {
    long hit = [openFiles indexOfObject:currentFile];
    
    if (hit >0) {
        hit--;
    }
    
    currentFile = [openFiles objectAtIndex:hit];
    
    [self updateFromDocument];
}


- (IBAction)dateChanged:(id)sender {
    if (currentFile != nil) {
        [currentFile setDocumentDate:[documentDateField dateValue]];
    }
}


- (IBAction)notebookPopupChanged:(id)sender {
    [currentFile setDocumentNotebook:[[notebookPopup selectedItem] title]];
    
}

- (IBAction)viewDocument:(id)sender {
    [[NSWorkspace sharedWorkspace] openURL: [currentFile documentUrl]];
    
}

- (IBAction)openDocument:(id)sender {
    
    
    // Create the File Open Dialog class.
    NSOpenPanel* openDlg = [NSOpenPanel openPanel];
    
    
    openDlg.showsHiddenFiles = NO;
    openDlg.canChooseDirectories = NO;
    openDlg.canCreateDirectories = YES;
    openDlg.allowsMultipleSelection = YES;
    openDlg.allowedFileTypes = @[@"pdf"];
    
    // Enable the selection of files in the dialog.
    [openDlg setCanChooseFiles:YES];
    
    // Enable the selection of directories in the dialog.
    [openDlg setCanChooseDirectories:YES];
    
    [openDlg beginSheetModalForWindow:[self window] completionHandler:^(NSInteger result) {
        
        if (result==NSOKButton) {
            NSArray* files = openDlg.URLs;
            for (NSURL *url in files) {
                [self application:nil openFile:[url relativePath]];
            }

        }
        
    }];
    
    
}



#pragma mark -

NSString *const kFocusedAdvancedControlIndex = @"FocusedAdvancedControlIndex";

- (NSInteger)focusedAdvancedControlIndex
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:kFocusedAdvancedControlIndex];
}

- (void)setFocusedAdvancedControlIndex:(NSInteger)focusedAdvancedControlIndex
{
    [[NSUserDefaults standardUserDefaults] setInteger:focusedAdvancedControlIndex forKey:kFocusedAdvancedControlIndex];
}

#pragma mark - Delegates


- (BOOL)tableView:(NSTableView *)aTableView shouldSelectRow:(NSInteger)rowIndex {
    
    if (rowIndex == -1) {
        [currentFile setDocumentType:nil];
    } else {
        [currentFile setDocumentType:[[documentTypeController types] objectAtIndex:rowIndex]];
        
    }
    [currentFile applyDocumentType: true];
    [self updateFromDocument];
    
    return true;
    
}



- (BOOL)validateMenuItem:(NSMenuItem *)item {
    
    if ([item action] == @selector(openPreferences:)){
        return true;
    }
    if ([item action] == @selector(openDocument:)){
        return true;
    }
    
    if ([openFiles count] == 0) {
        return false;
    };
    
    long hit = [openFiles indexOfObject:currentFile];
    
    
    if ([item action] == @selector(nextDocument:)){
        return (hit < ([openFiles count]-1) );
    }
    if ([item action] == @selector(previousDocument:)){
        return (hit>0);
    }
    if ([item action] == @selector(viewDocument:)){
        return  [self currentFile] != nil;
        
    }
    if ([item action] == @selector(discardDocument:)){
        return  [self currentFile] != nil;
        
    }
    if ([item action] == @selector(notebookChanged:)){
        return true;
    }
    return true;
    
}

-(BOOL)validateToolbarItem:(NSToolbarItem *)toolbarItem {
    if ([[toolbarItem itemIdentifier] isEqual:@"tbSettings"]) {
        return true;
    }
    
    if ([openFiles count] == 0) {
        return false;
    };
    
    long hit = [openFiles indexOfObject:currentFile];
    
    if ([[toolbarItem itemIdentifier] isEqual:@"tbPrevious"]) {
        return (hit>0);
    } else if ([[toolbarItem itemIdentifier] isEqual:@"tbNext"]) {
        return (hit < ([openFiles count]-1) );
        
    } else if ([[toolbarItem itemIdentifier] isEqual:@"tbEvernote"] || [[toolbarItem itemIdentifier] isEqual:@"tbView"]) {
        return  [self currentFile] != nil;
        
    } else if ([[toolbarItem itemIdentifier] isEqual:@"tbDelete"]) {
        return [self currentFile] != nil;
        
    }
    return true;
}

- (void)controlTextDidChange:(NSNotification *)aNotification {
    if (currentFile != nil) {
        [currentFile setDocumentTitle:[documentTitleField stringValue]];
        [currentFile setDocumentTags:[documentTagsField stringValue]];
        [currentFile setDocumentNotebook:[[notebookPopup selectedItem] title]];
    }
}


- (NSArray *)tokenField:(NSTokenField *)tokenFieldArg completionsForSubstring:(NSString *)substring indexOfToken:(NSInteger)tokenIndex indexOfSelectedItem:(NSInteger *)selectedIndex {
    NSMutableArray *suggestions = [[NSMutableArray alloc]init];
    NSArray *tags = [[PBEvernoteController sharedInstance] tagsAsStrings];
    
    for (NSString *s in tags) {
        if ([s hasPrefix:substring]){
            [suggestions addObject:s];
        }
    }
    
    
    return suggestions;
}

@end
