//
//  PBMainView.m
//  PaperBuddy
//
//  Created by Ralf Pfleghar on 26.02.13.
//  Copyright (c) 2013 Ralf Pfleghar. All rights reserved.
//

#import "PBMainView.h"

@implementation PBMainView

NSImage *bgImage;

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}


- (BOOL)mouseDownCanMoveWindow {
    return YES;
}

- (void)drawRect:(NSRect)dirtyRect {
    // Fill in background Color
    CGContextRef context = (CGContextRef) [[NSGraphicsContext currentContext] graphicsPort];
    CGContextSetRGBFillColor(context, 0.03,0.32,0.11,1);
    CGContextFillRect(context, NSRectToCGRect(dirtyRect));
}


//- (void)drawRect:(NSRect)dirtyRect
//{
//    NSRect bounds = [self bounds];
//    NSSize imageSize = [bgImage size];
//    
//    // start at max Y (top) so that resizing the window looks to be anchored at the top left
//    for ( float y = NSHeight(bounds) - imageSize.height; y >= -imageSize.height; y -= imageSize.height ) {
//        for ( float x = NSMinX(bounds); x < NSWidth(bounds); x += imageSize.width ) {
//            NSRect tileRect = NSMakeRect(x, y, imageSize.width, imageSize.height);
//            if ( NSIntersectsRect(tileRect, dirtyRect) ) {
//                NSRect destRect = NSIntersectionRect(tileRect, dirtyRect);
//                [bgImage drawInRect:destRect
//                         fromRect:NSOffsetRect(destRect, -x, -y)
//                        operation:NSCompositeSourceOver 
//                         fraction:1.0];
//            }
//        }
//    }
//}

- (void)awakeFromNib
{
//    bgImage = [[NSImage alloc] initWithContentsOfFile: [[NSBundle mainBundle] pathForResource:@"exclusive_paper" ofType:@"png"]];

    [self setNeedsDisplay:YES];
}


@end
