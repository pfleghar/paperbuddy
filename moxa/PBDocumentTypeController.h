//
//  PBDocumentTypes.h
//  PaperBuddy
//
//  Created by Ralf Pfleghar on 01.03.13.
//  Copyright (c) 2013 Ralf Pfleghar. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface PBDocumentTypeController : NSObject <NSTableViewDataSource>

@property (copy) NSMutableDictionary *currentType;
@property NSMutableArray *types;

+ (PBDocumentTypeController*)sharedInstance;
- (void) setCurrentTypeByNumber: (NSInteger) number;
- (NSInteger *) indexOfCurrentType;
- (void) addType;
- (void) updateCurrentType: (NSMutableDictionary *) newType;
- (void) setTypes: (NSMutableArray *) types;
- (NSMutableArray *) types;
- (void) synchronizeTypes;

@end
